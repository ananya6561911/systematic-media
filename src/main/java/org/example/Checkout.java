package org.example;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.util.List;

public class Checkout {
    private ChromeDriver driver;

    @BeforeMethod
    public void setUp() {
        //Setting system properties of ChromeDriver
        System.setProperty("web-driver.chrome.driver", "C://Users//SDS//Downloads//chromedriver-win64//chromedriver-win64//chromedriver.exe/");
        System.setProperty("web-driver.http.factory", "jdk-http-client");

        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }
    @Test
    public void testLogin() throws InterruptedException {
        System.out.println("opening PC Chandra Career Page");
        driver.get("https://systematixmedia.com");
        Thread.sleep(5000);

        driver.findElement(By.xpath("/html/body/header/section[1]/div/div/div[1]/div[3]/ul/li[3]/a")).click();
        System.out.println("Navigate to Account Login");

        driver.findElement(By.id("login_email"));
        WebElement username=driver.findElement(By.id("login_email"));
                System.out.println("entering the email");
        username.sendKeys("ananya@sundewsolutions.com");
        Thread.sleep(5000);

        WebElement password=driver.findElement(By.id("password"));
                System.out.println("entering the Password");
        password.sendKeys("WDlk^(32");
        Thread.sleep(5000);

        System.out.println("Click On submit Button");
        driver.findElement(By.id("login_btn")).click();

//Verify & Validate The Action
        System.out.println("verifying Login Credentials");
        String actualURL = "https://systematixmedia.com/";
        String expectedURL = driver.getCurrentUrl(); // It will fetch the current URL
        Assert.assertEquals(expectedURL, actualURL); // It will verify both the URL are equal
        Thread.sleep(5000);
// Locating the element
        WebElement e = driver.findElement(By.xpath("/html/body/header/section[1]/div/div/div[1]/div[3]/ul/li[3]/div"));
        System.out.println(e.getText());
        System.out.println("Loggedin Sucessfully :) ");
// Search product using search bar
        WebElement search;
        search = driver.findElement(By.className("search search-toggle"));
        search.sendKeys("MTP63HN/A");
        driver.findElement(By.id("search_btn")).click();

// Select the searched image
        driver.findElement(By.xpath("/html/body/main/div[1]/div/div/div/div[1]/a")).click();

        System.out.println("verifying searched product");
        String actualURL1 = "https://systematixmedia.com/iphone/iphone-15/iphone-15-256gb/iphone-15-256gb-black";
        String expectedURL1 = driver.getCurrentUrl(); // It will fetch the current URL
        Assert.assertEquals(expectedURL1, actualURL1); // It will verify both the URL are equal
        Thread.sleep(5000);
// Locating the element
        WebElement product = driver.findElement(By.xpath("/html/body/main/section[2]/div/div/div[3]/div[2]/h1"));
        System.out.println(product.getText());

        driver.findElement(By.id("addToCart")).click();

// Proceed to checkout
        driver.findElement(By.xpath("/html/body/main/section[3]/div/div[3]/div[2]/div/div/a[2]")).click();
    }}
