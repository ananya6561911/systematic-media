package org.example;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
public class ProductQuery {
    private WebDriver driver;

    @BeforeMethod
    public void setUp() {

        //Setting system properties of ChromeDriver
        System.setProperty("web-driver.chrome.driver", "C://Users//SDS//Downloads//chromedriver-win64//chromedriver-win64//chromedriver.exe/");
        System.setProperty("web-driver.http.factory", "jdk-http-client");

        driver = new ChromeDriver();
        driver.get("https://systematixmedia.com/send-your-query");
        driver.manage().window().maximize();
    }

    @Test
    public void testLogin() throws InterruptedException {

        System.out.println("Entering all the required details");

                System.out.println("entering the Name");
        WebElement username=driver.findElement(By.id("name"));
                username.sendKeys("Ananya Sundew Test");
        Thread.sleep(5000);

                System.out.println("entering the Phone Number");
        WebElement phoneNumber=driver.findElement(By.id("phone"));
                phoneNumber.sendKeys("8987818846");
        Thread.sleep(5000);

                System.out.println("entering the Email");
        WebElement email=driver.findElement(By.id("email"));
                email.sendKeys("prabhat@sundewsolutions.com");
        Thread.sleep(5000);

                System.out.println("entering the Address");
        WebElement subject=driver.findElement(By.id("address"));
                subject.sendKeys("Hi this is daily checklist from Sundew Solutions by Ananya Chatterjee :D ");
        Thread.sleep(5000);

                System.out.println("entering the Message");
        WebElement message=driver.findElement(By.id("message"));
                message.sendKeys("Hi this is daily checklist from Sundew Solutions by Ananya Chatterjee :D");
        Thread.sleep(5000);

        // Find the element that contains the CAPTCHA mathematical expression
        WebElement captchaElement1 = driver.findElement(By.cssSelector("#sendyourquery_form > div > div > div.form-flexsb.captcha > div.form-element.form-elementCaptcha > b:nth-child(1)"));
        WebElement captchaElement2 = driver.findElement(By.cssSelector("#sendyourquery_form > div > div > div.form-flexsb.captcha > div.form-element.form-elementCaptcha > b:nth-child(3)"));

        String captchaText1 = captchaElement1.getText();
        String captchaText2 = captchaElement2.getText();

        int result = solveCaptcha(captchaText1,captchaText2);
        WebElement captchaInput = driver.findElement(By.id("capt"));
        captchaInput.sendKeys(String.valueOf(result));

        Thread.sleep(2000);
        driver.findElement(By.id("send")).click();

        WebElement e = driver.findElement(By.id("success_query_send"));
                Thread.sleep(2000);
        System.out.println(e.getText());
        driver.quit();

    }
    private static int solveCaptcha(String captchaText1,String captchaText2) {

        String onlyNumber = captchaText2.replaceAll("[^0-9]", "");
        int operand1 = Integer.parseInt(captchaText1);
        int operand2 = Integer.parseInt(onlyNumber);
       char operator = '+';

        return switch (operator) {
            case '+' -> operand1 + operand2;
            case '-' -> operand1 - operand2;
            case '*' -> operand1 * operand2;
            case '/' -> operand1 / operand2;
            default -> throw new IllegalArgumentException("Invalid operator: " + operator);
        };
    }

}

