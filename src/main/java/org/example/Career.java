package org.example;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.util.List;

public class Career {
    private WebDriver driver;

    @BeforeMethod
    public void setUp() {

        //Setting system properties of ChromeDriver
        System.setProperty("web-driver.chrome.driver", "C://Users//SDS//Downloads//chromedriver-win64//chromedriver-win64//chromedriver.exe/");
        System.setProperty("web-driver.http.factory", "jdk-http-client");

        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @Test
    public void testLogin() throws InterruptedException {
        System.out.println("opening PC Chandra Career Page");
        driver.get("https://systematixmedia.com/career");
        Thread.sleep(5000);

        System.out.println("Entering all the required details");

        System.out.println("Entering position applying for");
        WebElement positionappliedfor=driver.findElement(By.id("position"));
        positionappliedfor.sendKeys("Daily check-list by Sun dew solutions");
        Thread.sleep(1000);

        System.out.println("entering the Name");
        WebElement username=driver.findElement(By.id("name"));
        username.sendKeys("Ananya Sundew Test");
        Thread.sleep(1000);

        System.out.println("entering the Email");
        WebElement email=driver.findElement(By.id("email"));
        email.sendKeys("ananya@sundewsolutions.com");
        Thread.sleep(1000);

        System.out.println("entering the Phone Number");
        WebElement phoneNumber=driver.findElement(By.id("phone"));
        phoneNumber.sendKeys("8987818847");
        Thread.sleep(1000);

        System.out.println("What is your qualifications");
        WebElement qualification=driver.findElement(By.id("qualification"));
        qualification.sendKeys("Hi this is daily checklist from Sundew Solutions by Ananya Chatterjee :D");
        Thread.sleep(1000);

        //Handle dropdown
        driver.findElement(By.id("experience")).click();

        List<WebElement> allOptions = driver.findElements(By.cssSelector("select option"));

        String option = "2 - 3 years";

        // Iterate the list using for loop

        for (WebElement allOption : allOptions) {

            if (allOption.getText().contains(option)) {

                allOption.click();

                System.out.println("clicked");

                break;

            }

        }

        System.out.println("Write your message here");
        WebElement comments=driver.findElement(By.id("message"));
        comments.sendKeys("Hi this is daily checklist from Sundew Solutions by Ananya Chatterjee :D");
        Thread.sleep(1000);

        driver.findElement(By.id("resume")).sendKeys("C://Users//USER//Downloads//IITK Security Testing.pdf");

        WebElement captchaElement1 = driver.findElement(By.className("num1"));
        WebElement captchaElement2 = driver.findElement(By.className("num2"));

        String captchaText1 = captchaElement1.getText();
        String captchaText2 = captchaElement2.getText();

        int result = solveCaptcha(captchaText1,captchaText2);
        WebElement captchaInput = driver.findElement(By.id("capt"));
        captchaInput.sendKeys(String.valueOf(result));

        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,10000)");

        Thread.sleep(3000);
        driver.findElement(By.id("career_submit")).click();

        WebElement e = driver.findElement(By.id("success_contactus"));
                Thread.sleep(2000);
        System.out.println(e.getText());
        driver.quit();

    }
    private static int solveCaptcha(String captchaText1,String captchaText2) {

        String onlyNumber = captchaText2.replaceAll("[^0-9]", "");
        int operand1 = Integer.parseInt(captchaText1);
        int operand2 = Integer.parseInt(onlyNumber);
        char operator = '+';

        return switch (operator) {
            case '+' -> operand1 + operand2;
            case '-' -> operand1 - operand2;
            case '*' -> operand1 * operand2;
            case '/' -> operand1 / operand2;
            default -> throw new IllegalArgumentException("Invalid operator: " + operator);
        };
    }

}