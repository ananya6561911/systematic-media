package org.example;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.util.List;

public class CustomerFeedback {
    private WebDriver driver;

    @BeforeMethod
    public void setUp() {

        //Setting system properties of ChromeDriver
        System.setProperty("web-driver.chrome.driver", "C://Users//SDS//Downloads//chromedriver-win64//chromedriver-win64//chromedriver.exe/");
        System.setProperty("web-driver.http.factory", "jdk-http-client");

        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @Test
    public void testLogin() throws InterruptedException {
        System.out.println("opening PC Chandra Career Page");
        driver.get("https://systematixmedia.com/feedback");
        Thread.sleep(5000);

        System.out.println("Entering all the required details");

        System.out.println("entering the Name");
        WebElement username=driver.findElement(By.id("name"));
        username.sendKeys("Ananya Sundew Test");
        Thread.sleep(1000);

        System.out.println("entering the Phone Number");
        WebElement phoneNumber=driver.findElement(By.id("phone"));
        phoneNumber.sendKeys("8987818847");
        Thread.sleep(1000);

        System.out.println("entering the email");
        WebElement email=driver.findElement(By.id("email"));
        email.sendKeys("prabhat@sundewsolutions.com");
        Thread.sleep(1000);

        driver.findElement(By.id("country")).click();
        List<WebElement> allOptions = driver.findElements(By.cssSelector("#country > option:nth-child(2)"));
        String option = "India";

        for (WebElement allOption : allOptions) {

            if (allOption.getText().contains(option)) {

                allOption.click();

                System.out.println("clicked");

                break;

            }
        }

        driver.findElement(By.id("store_location_id")).click();
        List<WebElement> allOptions1 = driver.findElements(By.cssSelector("#store_location_id > option:nth-child(5)"));
        String option1 = "Acropolis Mall";

        for (WebElement allOption1 : allOptions1) {

            if (allOption1.getText().contains(option1)) {

                allOption1.click();

                System.out.println("clicked");

                break;
            }
        }

                driver.findElement(By.id("sales_advisor")).click();
        List<WebElement> allOptions2 = driver.findElements(By.cssSelector("#sales_advisor > option:nth-child(3)"));
        String option2 = "Debabrata Sinha";

        for (WebElement allOption2 : allOptions2) {

            if (allOption2.getText().contains(option2)) {

                allOption2.click();

                System.out.println("clicked");

                break;
            }
        }



        System.out.println("Enter Address");
        WebElement Address=driver.findElement(By.id("address"));
        Address.sendKeys("Daily check-list by Sun dew solutions");
        Thread.sleep(1000);

        System.out.println("Write your message here");
        WebElement comments=driver.findElement(By.id("message"));
        comments.sendKeys("Hi this is daily checklist from Sundew Solutions by Ananya Chatterjee :D");
        Thread.sleep(1000);

        WebElement captchaElement1 = driver.findElement(By.className("num1"));
        WebElement captchaElement2 = driver.findElement(By.className("num2"));

        String captchaText1 = captchaElement1.getText();
        String captchaText2 = captchaElement2.getText();

        int result = solveCaptcha(captchaText1,captchaText2);
        WebElement captchaInput = driver.findElement(By.id("capt"));
        captchaInput.sendKeys(String.valueOf(result));

        Thread.sleep(2000);
        driver.findElement(By.id("send")).click();

        driver.quit();

    }
    private static int solveCaptcha(String captchaText1,String captchaText2) {

        String onlyNumber = captchaText2.replaceAll("[^0-9]", "");
        int operand1 = Integer.parseInt(captchaText1);
        int operand2 = Integer.parseInt(onlyNumber);
       char operator = '+';

        return switch (operator) {
            case '+' -> operand1 + operand2;
            case '-' -> operand1 - operand2;
            case '*' -> operand1 * operand2;
            case '/' -> operand1 / operand2;
            default -> throw new IllegalArgumentException("Invalid operator: " + operator);
        };
    }

}